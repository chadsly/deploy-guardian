variable "AWS_ACCESS_KEY" { default = "" }

variable "AWS_SECRET_KEY" { default = "" }

variable "NEXUS_USERNAME" { default = "" }

variable "NEXUS_PASSWORD" { default = "" }