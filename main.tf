provider "aws" {
  alias      = "gov-west"
  region     = "us-gov-west-1"
  access_key = var.AWS_ACCESS_KEY
  secret_key = var.AWS_SECRET_KEY
}

module "template" {
  source  = "terraform.cie.vi2e.io/High-Altitude/high-altitude/aws"
  providers = {
    aws = aws.gov-west
  }
  ec2_size          = "t2.small"
  aws_ami           = "ami-5a740e3b"#<-RHEL   Ubuntu->"ami-adecdbcc"
  ssh_cidrs         = [ "10.0.0.0/8"]
  #Environment Specific
  vpc_id            = "vpc-8ad750ee"
  public_subnets    = ["subnet-3b37774d", "subnet-2f3e7e4b", "subnet-e27dd9bb"]
  private_subnets   = ["subnet-9d3d7deb", "subnet-2c3b7b48", "subnet-5468cc0d"]
  #User Specific
  app_download_url  = "https://nexus.di2e.net/nexus3/repository/Private_OADCGS-GEOINT/Server/Guardian/1.5.17.1342/Guardian-1.5.17.1342.zip"
  app_version       = "Guardian-1.5.17.1342"
  app_tpl_filepath_filename = "templates/app.sh.tpl"
  NEXUS_USERNAME    = var.NEXUS_USERNAME
  NEXUS_PASSWORD    = var.NEXUS_PASSWORD
  http_proxy        = "http://internal-transit-c-rELBProx-1FIWKJ1YWJ83C-1424253041.us-gov-west-1.elb.amazonaws.com:3128/"
  no_proxy          = "'localhost,http://127.0.0.1,10.0.0.0/8'"
  app_count         = 3
  alb_port          = "443"
  ## List of Variables for initialization
  application_name  = "guardian"
}